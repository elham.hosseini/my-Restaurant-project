## Restaurant Reserve Scenario

### Scenario uses restaurant reservation system

* **Goal:**  Scenario that say The use of this app for Restaurant

* **Users:**  Restaurant 'A' In Rafsanjan

* **Equipment:** a computer

* Scenario:

1. Employee install app 
2. Employee 'A' after seeing  login page , insert user & pass that fix by admin.
3. Employee 'A'  after inter  main page ,see date and customer numbers.
4. Employee 'A' With the arrival of the first customer, choose food from list and pay money , employee gives the bill to hem.


## scenario New food  and its price

* **Goal:**  Scenario entry the new food and its price
* اشخاص :مدیر رستوران الف از شهر رفسنجان
 
* تجهیزات : کامپیوتری دارای  سیستم عامل ویندوز و دیتا بیس اس کیو ال و ماشین مجازی جی آر ای

* سناریو :
 
1. مدیر نرم افزار را روی سیستم خود نصب میکند 

2. مدیر با مشاهده ی صفحه ی لاگین و انتخاب آیکون ورود مدیر وارد میشود و یوزر و پسوورد  که توسط برناه نویس نرم افزار مشخص شده را وارد میکند 
 
3. مدیر در صفحه ی روبه رو ی خود  علاوه بر مشاهده غذا ها و قیمت آن و در صورت خواستن به عوض کردن هر کدام گزینه اتصحیح را انتخاب میکند و نام و قیمت جدید را ورد میکند و همچنین میتواند آن را حذف کند و در پایان گزینه اتمام را کلیک میکند و به صفحه ی نخست بازمیکدردد

4. مدیر با ورود به صفحه ی نخست میتواند با کلیک کردن روی ایکون "افزودن"غذای جدید با قیمت آ« را وارد کند و آیکون اتمام را میزند.
 
5. و مدیر در اتمام کار خود گزینه اتمام و خروج را میفشارد.


# سناریو گزارش گیری

* هدف: گزارش گیری هفتگی
 
* اشخاص:مدیر رستوران الف از شهر رفسنجان

* تجهیزات : کامپیوتری دارای  سیستم عامل ویندوز و دیتا بیس اس کیو ال و ماشین مجازی جی آر ای

* سناریو :

1. مدیر نرم افزار را روی سیستم خود نصب میکند 

2. مدیر با مشاهده ی صفحه ی لاگین و انتخاب آیکون ورود مدیر وارد میشود و یوزر و پسوورد  که توسط برناه نویس نرم افزار مشخص شده را وارد میکند 

3. مدیر روی آیکون گزارش گیری کلیک میکند
 
4. مدیر علاوه بر مشاهده لیست فروش هفتگی با کلیک کردن روی چاپ میتواند آنهارا چاپ کند